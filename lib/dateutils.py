import re


def split_partial_date(partial_date: str) -> list:
    """Split a partial date into a list of parts .

    Args:
        partial_date (str): a partial date formatted YYYY-mm

    Returns:
        list: returns the year and the month in a list
    """
    if bool(re.match(r"^\d{4}(\/)\d{2}$", string=partial_date)):
        return partial_date.split("/")
    raise ValueError("The date pattern does not match YYYY/MM")
