import pytest

from .dateutils import split_partial_date


@pytest.mark.parametrize(
    "__input",
    [
        ("ahahah"),
        ("2021-11"),
        ("26/08/1983"),
    ],
)
def test_partialdate_split(__input):
    """Test wrong dates raises an exception.

    Args:
        __input ([type]): [description]
    """
    with pytest.raises(ValueError):
        split_partial_date(__input)


def test_right_format_yield():
    """
    Tests that the method yields what is expected
    given the right parameters
    """
    assert isinstance(split_partial_date("1983/08"), list)
    assert len(split_partial_date("1983/08")) == 2
    for n in split_partial_date("1983/08"):
        assert isinstance(n, str)
