"""
Yaml config reader utilities
"""
import os

import yaml


def read_yaml(path: str) -> dict:
    """Reads a yaml file and returns it as a dictionary .

    Args:
        path (str): The path of the yaml config file to read from.

    Returns:
        dict: a dictionary with the content of the yaml config file.
        or FileNotFoundError
    """
    if not os.path.isfile:
        raise FileNotFoundError
    with open(path, "r") as infile:
        return yaml.safe_load(infile)
