import pytest

from .yamlreader import read_yaml

CFG = read_yaml("lib/testing_resources/test.yaml")


def test_yaml_reader_raise_exception():
    """Tests that a wrong path raises FileNotFoundError."""
    with pytest.raises(FileNotFoundError):
        read_yaml("fake/path.yaml")


def test_universal_answer():
    """Test whether the user answer is the answer."""
    assert CFG["answer"] == 42
