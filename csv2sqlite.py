from lib import read_yaml
import sys
from lib import split_partial_date
import argparse
import requests
import os
from csv import reader
import sqlite3

if __name__ == "__main__":
    # Check parsing
    PARSER = argparse.ArgumentParser(description="CSV files processor")
    PARSER.add_argument(
        "-D",
        "--date",
        required=True,
        help="The day and month to be retrieved from S3 storage",
    )
    PARSER.add_argument(
        "--nodl",
        required=False,
        help="Use this option to avoid download the file again",
        action="store_true"
    )
    PARSER.add_argument(
        "--keepcsv",
        required=False,
        help="Use this option to avoid removing the file at the end of the process",
        action="store_true"
    )
    PARSER.add_argument(
        "--purge",
        required=False,
        help="""Use this option to remove the files and the
        database from storage. The program is terminated after this action.""",

        action="store_true"
    )
    # CONSTANTS
    ARGS = PARSER.parse_args()
    CONFIG = read_yaml("./csv2sqlite_config.yaml")
    STORAGE_PATH = CONFIG["storage_path"]
    STAGING_PATH = f"{STORAGE_PATH}/staging"
    DB_PATH = f"{STORAGE_PATH}/db"
    SPLITTED = split_partial_date(ARGS.date)
    YEAR, MONTH = SPLITTED[0], SPLITTED[1]
    FILE_SUFFIX = CONFIG["remote_files_suffix"]
    FULL_URL = os.path.join(
        CONFIG["bucket_url"],
        YEAR,
        MONTH,
        FILE_SUFFIX,
    )
    LOCAL_FILE_NAME = f"{STAGING_PATH}/{YEAR}_{MONTH}_{FILE_SUFFIX}"
    if ARGS.purge:
        # removing database and any remaining file from local storage
        os.remove(LOCAL_FILE_NAME)
        os.remove(os.path.join(DB_PATH, CONFIG["records_database_name"]),)
        sys.exit(0)

    print(f"downloading {FULL_URL} into {LOCAL_FILE_NAME}")
    
    # check if storage path exists
    if not os.path.isdir(STORAGE_PATH):
        # if the dir does not exist, it will be created
        os.makedirs(STAGING_PATH, exist_ok=True)
        os.makedirs(DB_PATH, exist_ok=True)
    # create database connection, init cursor
    conn = sqlite3.connect(
        os.path.join(DB_PATH, CONFIG["records_database_name"]),
    )
    c = conn.cursor()
    print("Downloading resource into staging area, Please wait...")
    # make request
    if not ARGS.nodl:
        # in case of error, no need to dl it again
        r = requests.get(FULL_URL, allow_redirects=True)
        # if the return code is not 200
        if r.status_code != 200:
            # display message and exit
            print("An error occured while trying to download resource.")
            print("The remote file does not seem to exist.")
            sys.exit(1)

        else:
            # attempt to write temporary file
            try:
                open(
                    LOCAL_FILE_NAME,
                    "wb",
                ).write(r.content)
            except Exception as e:
                print("An error occured while trying to download resource.")
                print(f"Error : {e}.")
                sys.exit(1)
        # open file
    with open(LOCAL_FILE_NAME, "r") as csv_file:
        # extract header
        csv_reader = reader(csv_file)
        header = next(csv_reader)
        # create the table if necessary with the values of the header
        # using BLOB to ensure compatibility
        table_creation = f'''CREATE TABLE IF NOT EXISTS
         {CONFIG["records_table_name"]} ({", TEXT".join(header)});'''
        # exec and commit transaction
        c.execute(table_creation)
        conn.commit()
        print("Inserting data")
        # iterate over csv lines minus the header
        for cpt, row in enumerate(csv_reader):
            if cpt % 1000 == 0:
                # print situation every 1000 lines
                print(f"{cpt} lines processed")
            # change char separator for the last element of the list
            row[-1] = row[-1].replace(",", "|")
            # also remove - from the first one
            row[0] = row[0].replace("-", "")
            insertion = f'INSERT INTO {CONFIG["records_table_name"]} VALUES({",".join(["?" for x in row])});'
            c.execute(insertion, row)
            if cpt % 100 == 0:
                # commit every 100 lines (attempt to gain time)
                """TODO assess if commit every x lines vs commit every line is efficient
                both in term of time and performances
                """
                conn.commit()
        # trigger commit once more in case few processed lines were not
        # commited because of the modulo count
        conn.commit()
    print("Removing file from the system")
    if os.path.exists(LOCAL_FILE_NAME) and not ARGS.keepcsv:
        os.remove(LOCAL_FILE_NAME)
    print("Deleting resource")
    print("Finished")
