
# csv2sqlite

---

This script downloads some S3 files and inserts the lines it contains into the
sqlite database.

## Prerequisites

- Python 3.6+ (virtual) environment
- pipenv
- read and edit csv2sqlite_config.yaml

## Installation

1. Enter the directory containing the environment
2. ```$ pipenv shell``` to to start the virtual environment
3. ```$ pipenv install``` to run the installer (packages from pipfile)
4. [optional] install dev packages (linters,chackers,testing, ...) ```$ pipenv install --dev [--pre]```

## Running tests

```$ pytest .```

## Running code

With the virtual environment started :

```$ python csv2sqlite.py -D/--date YYYY/MM [--nodl] [--keep] [--purge] [-h]````

The process will stop if the response code is not 200,
either because the URL is malformed or because the
desired resource does not exist

- -D/--date : required, must follow the pattern YYYY/MM
- --nodl : The file will not be downloaded again (useful in test situation)
- --keep : The csv file will be kept (also useful in test situation)
- --purge : All the data contained into the storage dir will be terminated.
- -h : display help

## Browsing data

You can browse the data contained in the database using
sqlitebrowser (GNU/Linux package)

- using apt:
```$ [sudo] apt install sqlitebrowser```
```$ sqlitebrowser /path/to/database/file```

## Q&A

- Why requests ?

Because it's easy and because boto3 did not worked properly.
It seems that boto3 does not like the lack of credentials event if the auth
is deactivated.

- Why sqlite3

Sqlite3 is a built-in package so, there is no need to worry about compatibility
regarding the python version used.
It is also extremely lightweight and does not require external installation.
I could have used mongo but it would have been a tad more complicated because
a model is necessary.

## Things that can be added in the future and why

- the ability to process a range of dates

Something like ```python csv2sqlite.py --start YYYY/MM --end YYYY/MM``` could
be useful to save time in case of multiple files to process

- The ability to provide a header

In case the header is missing, a simple option like
```-H\ --header xxx,xxx,xxx,xxx,xxx,...``` could do the job.
This could also replace an existing header.

- An audit option.

An audit option could be added to be able to view the header and a sample
of the csv file and count the lines (with wc -l) .

- Better indexing for sqlite using UNIQUE for IDs.

To avoid inserting the same info twice and prevent data collision

- Ensure mutually exclusive arguments (ARGPARSE).
- Move --purge code into another file.
- Move argparse options into a yaml file.
